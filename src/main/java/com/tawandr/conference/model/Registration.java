package com.tawandr.conference.model;

/**
 * Created By Dougie T Muringani : 1/12/2020
 */
public class Registration {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
