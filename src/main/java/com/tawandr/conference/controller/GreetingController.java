package com.tawandr.conference.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

/**
 * Created By Dougie T Muringani : 1/12/2020
 */
@Controller
public class GreetingController {
    @GetMapping("greeting")
    public String greeting(Map<String, Object> model){
        model.put("message", "Hello Doug");
        return "greeting";
    }
}
